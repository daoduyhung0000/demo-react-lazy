import img from './assets/pexels-roberto-nickson-2559941.jpg'

function Image() {
    console.log(img)
    return(
        <div style={{
            background: `url(${img})`,
            width: '500px',
            height: '500px',
            backgroundSize: 'cover'
        }}></div>
    )
}

export default Image