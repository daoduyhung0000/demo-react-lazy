import React,  { Suspense } from 'react';
import url from './assets/pexels-cesar-perez-733745.jpg'
const Pic = React.lazy(()=> import('./Image'))

function App() {
  return (
    <Suspense fallback={<div
      style={{
        background: `url(${url})`,
        width: '500px',
        height: '500px',
        backgroundSize:'cover'
      }}
    ></div>}>
      <Pic/>
    </Suspense>
  );
}

export default App;
